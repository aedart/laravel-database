<?php

use Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper;
use Aedart\Laravel\Database\Migrations\Packages\MigratorHelper;
use Aedart\Testing\Laravel\Traits\ApplicationInitiatorTrait;
use Codeception\Configuration;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\Schema;

/**
 * Class MigrationHelperTest
 *
 * @coversDefaultClass Aedart\Laravel\Database\Migrations\Packages\MigratorHelper
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class MigrationHelperTest extends \Codeception\TestCase\Test
{
    use ApplicationInitiatorTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * List of loaded migration files - for debugging
     *
     * @var string[]
     */
    protected $loadedFilesList = [];

    /**
     * Name of the custom vendor directory
     *
     * @var string
     */
    protected $customVendorDirectoryName = 'myVendorLib';

    /**
     * Full path to the custom vendor directory
     *
     * @var string
     */
    protected $pathToVendor = null;

    protected function _before()
    {
        $this->pathToVendor = Configuration::dataDir() . '/' . $this->customVendorDirectoryName;

        //$this->loadMigrationFiles();
        $this->startApplication();
    }

    protected function _after()
    {
        $this->stopApplication();
    }

    /******************************************************************************
     * Helpers
     *****************************************************************************/

    /**
     * Get a new instance of a migrations helper class
     *
     * @return IMigratorHelper
     */
    protected function getMigrationHelper(){
        return new MigratorHelper();
    }

    /**
     * Load the migration php files
     *
     * Normally, this would be automatically be resolved by composer,
     * when defining a autoload, e.g. class map, etc. However, in our
     * case, because its a test, we need to do so manually
     */
    protected function loadMigrationFiles(){
        foreach(Finder::create()->files()->in($this->pathToVendor) as $file){
            $fullPath = $file->getRealpath();
            require_once $fullPath;
            $this->loadedFilesList[] = $fullPath;
        }
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

//    /**
//     * @test
//     */
//    public function migrationFilesAreLoaded(){
//        $this->assertNotEmpty($this->loadedFilesList, 'Migration files need to be required manually, not have been loaded!');
//    }

    /**
     * @test
     * @covers ::runMigrationList
     * @covers ::runMigration
     * @covers ::prepare
     */
    public function runMigrationNonRecursive(){
        $helper = $this->getMigrationHelper();
        $helper->runMigration($this->pathToVendor . '/acme/db/src/migrations', false);

        $this->assertTrue(Schema::hasTable('products'), 'products table not created');
    }

    /**
     * @test
     * @covers ::rollBackLastMigration
     * @depends runMigrationNonRecursive
     */
    public function rollBackLastMigration(){
        $helper = $this->getMigrationHelper();
        $this->assertSame(1, $helper->rollBackLastMigration());
    }

    /**
     * @test
     * @covers ::runMigrationList
     * @covers ::runMigration
     * @covers ::prepare
     *
     * @covers ::getAndLoadMigrationFiles
     * @covers ::getAllNestedDirectoriesFromList
     * @covers ::getAllNestedDirectoriesFrom
     * @covers ::loadMigrationFiles
     * @covers ::requireFile
     */
    public function runMigrationRecursive(){
        $helper = $this->getMigrationHelper();
        $helper->runMigration($this->pathToVendor . '/acme/db/src/migrations');

        $this->assertTrue(Schema::hasTable('products'), 'products table not created');
        $this->assertTrue(Schema::hasTable('users'), 'users table not created');
        $this->assertTrue(Schema::hasTable('sales'), 'sales table not created');
    }

    /**
     * @test
     * @covers ::rollBackAllMigrations
     * @depends runMigrationRecursive
     */
    public function rollBackAllMigrations(){
        $helper = $this->getMigrationHelper();
        $this->assertSame(3, $helper->rollBackAllMigrations());
    }

    /**
     * @test
     * @covers ::runMigrationList
     * @covers ::rollBackAllMigrations
     */
    public function runMigrationListNonRecursive(){
        $helper = $this->getMigrationHelper();
        $helper->runMigrationList([
            $this->pathToVendor . '/acme/db/src/migrations',
            $this->pathToVendor . '/acme/plugin/src/myMigrations',
        ], false);

        $this->assertTrue(Schema::hasTable('products'), 'products table not created');
        $this->assertTrue(Schema::hasTable('plugin'), 'plugin table not created');
        $this->assertSame(2, $helper->rollBackAllMigrations());
    }

    /**
     * @test
     * @covers ::runMigrationList
     * @covers ::rollBackAllMigrations
     */
    public function runMigrationListRecursive(){
        $helper = $this->getMigrationHelper();
        $helper->runMigrationList([
            $this->pathToVendor . '/acme/db/src/migrations',
            $this->pathToVendor . '/acme/plugin/src/myMigrations',
        ]);

        // Debug
        //$this->fail(print_r($helper->getNotes(), true));

        $this->assertTrue(Schema::hasTable('products'), 'products table not created');
        $this->assertTrue(Schema::hasTable('plugin'), 'plugin table not created');
        $this->assertTrue(Schema::hasTable('users'), 'users table not created');
        $this->assertTrue(Schema::hasTable('sales'), 'sales table not created');
        $this->assertTrue(Schema::hasColumn('sales','user_id'), 'sales table not updated with new foreign key relation');
        $this->assertSame(5, $helper->rollBackAllMigrations());
    }

    /**
     * @test
     * @covers ::runPackageMigration
     * @covers ::runPackageMigrations
     * @covers ::rollBackAllMigrations
     */
    public function runPackageMigration(){
        $helper = $this->getMigrationHelper();
        $helper->setVendorPath($this->pathToVendor);
        $helper->runPackageMigration('/acme/plugin/src/myMigrations', false);

        $this->assertTrue(Schema::hasTable('plugin'), 'plugin table not created');
        $this->assertSame(1, $helper->rollBackAllMigrations());
    }

    /**
     * @test
     * @covers ::runPackageMigrations
     * @covers ::rollBackAllMigrations
     */
    public function runMultiplePackageMigrations(){
        $helper = $this->getMigrationHelper();
        $helper->setVendorPath($this->pathToVendor);
        $helper->runPackageMigrations([
            '/acme/db/src/migrations',
            '/acme/plugin/src/myMigrations',
        ]);

        $this->assertTrue(Schema::hasTable('products'), 'products table not created');
        $this->assertTrue(Schema::hasTable('plugin'), 'plugin table not created');
        $this->assertTrue(Schema::hasTable('users'), 'users table not created');
        $this->assertTrue(Schema::hasTable('sales'), 'sales table not created');
        $this->assertTrue(Schema::hasColumn('sales','user_id'), 'sales table not updated with new foreign key relation');
        $this->assertSame(5, $helper->rollBackAllMigrations());
    }

    /**
     * @test
     * @covers ::getNotes
     */
    public function getNotes(){
        $helper = $this->getMigrationHelper();
        $helper->setVendorPath($this->pathToVendor);
        $helper->runPackageMigrations([
            '/acme/db/src/migrations',
            '/acme/plugin/src/myMigrations',
        ]);

        $this->assertCount(5, $helper->getNotes(), 'Invalid amount of notes returned');
        $this->assertSame(5, $helper->rollBackAllMigrations());
    }
}