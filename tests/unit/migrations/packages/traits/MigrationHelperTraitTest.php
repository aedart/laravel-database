<?php

use Aedart\Laravel\Database\Migrations\Packages\Interfaces\MigratorHelperAware;
use Aedart\Laravel\Database\Migrations\Packages\Traits\MigratorHelperTrait;
use Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper;
use \Mockery;
use Aedart\Testing\Laravel\Traits\ApplicationInitiatorTrait;

/**
 * Class MigrationHelperTraitTest
 *
 * @coversDefaultClass Aedart\Laravel\Database\Migrations\Packages\Traits\MigratorHelperTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class MigrationHelperTraitTest extends \Codeception\TestCase\Test
{
    use ApplicationInitiatorTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $this->startApplication();
    }

    protected function _after()
    {
        Mockery::close();
        $this->stopApplication();
    }

    /******************************************************************************
     * Helpers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Laravel\Database\Migrations\Packages\Interfaces\MigratorHelperAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Laravel\Database\Migrations\Packages\Traits\MigratorHelperTrait');
        return $m;
    }

    /**
     * Get dummy implementation
     *
     * @return Aedart\Laravel\Database\Migrations\Packages\Interfaces\MigratorHelperAware
     */
    protected function getDummyImpl(){
        return new DummyMigratorHelperClass();
    }

    /**
     * Get a migrator helper mock
     *
     * @return Mockery\MockInterface|Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper
     */
    protected function getMigratorHelperMock(){
        $m = Mockery::mock('Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::getMigratorHelper
     * @covers ::hasMigratorHelper
     * @covers ::hasDefaultMigratorHelper
     * @covers ::setMigratorHelper
     * @covers ::isMigratorHelperValid
     * @covers ::getDefaultMigratorHelper
     */
    public function getDefaultMigratorHelper(){
        $trait = $this->getTraitMock();
        $this->assertInstanceOf('Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper', $trait->getMigratorHelper());
    }

    /**
     * @test
     * @covers ::hasDefaultMigratorHelper
     * @covers ::getDefaultMigratorHelper
     */
    public function getNullAsDefaultMigratorHelper(){
        $dummy = $this->getDummyImpl();
        $this->assertFalse($dummy->hasDefaultMigratorHelper());
        $this->assertNull($dummy->getDefaultMigratorHelper());
    }

    /**
     * @test
     * @covers ::getMigratorHelper
     * @covers ::hasMigratorHelper
     * @covers ::hasDefaultMigratorHelper
     * @covers ::setMigratorHelper
     * @covers ::isMigratorHelperValid
     */
    public function setAndGetMigratorHelper(){
        $trait = $this->getTraitMock();

        $helper = $this->getMigratorHelperMock();
        $helper->shouldReceive('hasMigrator')
            ->andReturn(false);
        $helper->shouldReceive('hasDefaultMigrator')
            ->andReturn(true);

        $trait->setMigratorHelper($helper);

        $this->assertSame($helper, $trait->getMigratorHelper());
    }

    /**
     * @test
     * @covers ::getMigratorHelper
     * @covers ::hasMigratorHelper
     * @covers ::hasDefaultMigratorHelper
     * @covers ::setMigratorHelper
     * @covers ::isMigratorHelperValid
     *
     * @expectedException \Aedart\Laravel\Database\Migrations\Packages\Exceptions\InvalidMigratorHelperException
     */
    public function attemptSetInvalidMigratorHelper(){
        $trait = $this->getTraitMock();

        $helper = $this->getMigratorHelperMock();
        $helper->shouldReceive('hasMigrator')
            ->andReturn(false);
        $helper->shouldReceive('hasDefaultMigrator')
            ->andReturn(false);

        $trait->setMigratorHelper($helper);
    }
}

class DummyMigratorHelperClass implements MigratorHelperAware {
    use MigratorHelperTrait;

    public function getDefaultMigratorHelper() {
        return null;
    }
}