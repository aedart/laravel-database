<?php

use Aedart\Laravel\Database\Migrations\Interfaces\MigratorAware;
use Aedart\Laravel\Database\Migrations\Traits\MigratorTrait;
use Aedart\Testing\Laravel\Traits\ApplicationInitiatorTrait;
use Illuminate\Database\Migrations\Migrator;
use \Mockery;

/**
 * Class MigratorTraitTest
 *
 * @coversDefaultClass Aedart\Laravel\Database\Migrations\Traits\MigratorTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class MigratorTraitTest extends \Codeception\TestCase\Test
{
    use ApplicationInitiatorTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Helpers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Laravel\Database\Migrations\Interfaces\MigratorAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Laravel\Database\Migrations\Traits\MigratorTrait');
        return $m;
    }

    /**
     * Get a dummy implementation
     *
     * @return Aedart\Laravel\Database\Migrations\Interfaces\MigratorAware
     */
    protected function getDummyImpl(){
        return new DummyMigratorClass();
    }

    /**
     * Get a migrator mock
     *
     * @return Mockery\MockInterface|Illuminate\Database\Migrations\Migrator
     */
    protected function getMigratorMock(){
        $m = Mockery::mock('Illuminate\Database\Migrations\Migrator');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultMigrator
     * @covers ::getDefaultMigrator
     */
    public function getNullAsDefaultMigrator(){
        $trait = $this->getTraitMock();
        $this->assertFalse($trait->hasDefaultMigrator());
        $this->assertNull($trait->getDefaultMigrator());
    }

    /**
     * @test
     * @covers ::getMigrator
     * @covers ::hasMigrator
     * @covers ::hasDefaultMigrator
     * @covers ::setMigrator
     * @covers ::getDefaultMigrator
     * @covers ::isMigratorValid
     */
    public function getDefaultLaravelMigrator(){
        $this->startApplication();

        $trait = $this->getTraitMock();
        $this->assertInstanceOf('Illuminate\Database\Migrations\Migrator', $trait->getMigrator());

        $this->stopApplication();
    }

    /**
     * @test
     * @covers ::getMigrator
     * @covers ::hasMigrator
     * @covers ::setMigrator
     * @covers ::isMigratorValid
     */
    public function setAndGetMigrator(){
        $trait = $this->getTraitMock();

        $migrator = $this->getMigratorMock();
        $trait->setMigrator($migrator);

        $this->assertSame($migrator, $trait->getMigrator());
    }

    /**
     * @test
     * @covers ::getMigrator
     * @covers ::hasMigrator
     * @covers ::setMigrator
     * @covers ::isMigratorValid
     *
     * @expectedException \Aedart\Laravel\Database\Migrations\Exceptions\InvalidMigratorException
     */
    public function attemptSetInvalidMigrator(){
        $dummy = $this->getDummyImpl();

        $migrator = $this->getMigratorMock();
        $dummy->setMigrator($migrator);
    }
}

class DummyMigratorClass implements MigratorAware {
    use MigratorTrait;

    public function isMigratorValid(Migrator $migrator) {
        return false;
    }
}