<?php

use Aedart\Laravel\Database\Migrations\Interfaces\MigrationRepositoryAware;
use Aedart\Laravel\Database\Migrations\Traits\MigrationRepositoryTrait;
use Illuminate\Database\Migrations\MigrationRepositoryInterface;
use Aedart\Testing\Laravel\Traits\ApplicationInitiatorTrait;
use \Mockery;

/**
 * Class MigrationRepositoryTraitTest
 *
 * @coversDefaultClass Aedart\Laravel\Database\Migrations\Traits\MigrationRepositoryTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class MigrationRepositoryTraitTest extends \Codeception\TestCase\Test
{
    use ApplicationInitiatorTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Helpers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Laravel\Database\Migrations\Interfaces\MigrationRepositoryAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Laravel\Database\Migrations\Traits\MigrationRepositoryTrait');
        return $m;
    }

    /**
     * Get a dummy implementation
     *
     * @return Aedart\Laravel\Database\Migrations\Interfaces\MigrationRepositoryAware
     */
    protected function getDummyImpl(){
        return new DummyMigrationRepositoryClass();
    }

    /**
     * Get a migration repository mock
     *
     * @return Mockery\MockInterface|Illuminate\Database\Migrations\MigrationRepositoryInterface
     */
    protected function getMigrationRepositoryMock(){
        $m = Mockery::mock('Illuminate\Database\Migrations\MigrationRepositoryInterface');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultMigrationRepository
     * @covers ::getDefaultMigrationRepository
     */
    public function getNullAsDefaultMigrationRepository(){
        $trait = $this->getTraitMock();
        $this->assertFalse($trait->hasDefaultMigrationRepository());
        $this->assertNull($trait->getDefaultMigrationRepository());
    }

    /**
     * @test
     * @covers ::getMigrationRepository
     * @covers ::hasMigrationRepository
     * @covers ::hasDefaultMigrationRepository
     * @covers ::setMigrationRepository
     * @covers ::getDefaultMigrationRepository
     * @covers ::isMigrationRepositoryValid
     */
    public function getDefaultMigrationRepository(){
        $this->startApplication();

        $trait = $this->getTraitMock();
        $this->assertInstanceOf('Illuminate\Database\Migrations\MigrationRepositoryInterface', $trait->getMigrationRepository());

        $this->stopApplication();
    }

    /**
     * @test
     * @covers ::getMigrationRepository
     * @covers ::hasMigrationRepository
     * @covers ::setMigrationRepository
     * @covers ::isMigrationRepositoryValid
     */
    public function setAndGetMigrationRepository(){
        $trait = $this->getTraitMock();

        $repository = $this->getMigrationRepositoryMock();

        $trait->setMigrationRepository($repository);

        $this->assertSame($repository, $trait->getMigrationRepository());
    }

    /**
     * @test
     * @covers ::setMigrationRepository
     * @covers ::isMigrationRepositoryValid
     *
     * @expectedException \Aedart\Laravel\Database\Migrations\Exceptions\InvalidMigrationRepositoryException
     */
    public function attemptSetInvalidMigrationRepository(){
        $dummy = $this->getDummyImpl();

        $repository = $this->getMigrationRepositoryMock();

        $dummy->setMigrationRepository($repository);
    }
}

class DummyMigrationRepositoryClass implements MigrationRepositoryAware {
    use MigrationRepositoryTrait;

    public function isMigrationRepositoryValid(MigrationRepositoryInterface $repository) {
        return false;
    }
}