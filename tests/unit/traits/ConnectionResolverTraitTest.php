<?php

use Aedart\Laravel\Database\Interfaces\ConnectionResolverAware;
use Aedart\Laravel\Database\Traits\ConnectionResolverTrait;
use Aedart\Testing\Laravel\Traits\ApplicationInitiatorTrait;
use Illuminate\Database\ConnectionResolverInterface;
use \Mockery;

/**
 * Class ConnectionResolverTraitTest
 *
 * @coversDefaultClass Aedart\Laravel\Database\Traits\ConnectionResolverTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ConnectionResolverTraitTest extends \Codeception\TestCase\Test
{
    use ApplicationInitiatorTrait;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Helpers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Laravel\Database\Interfaces\ConnectionResolverAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Laravel\Database\Traits\ConnectionResolverTrait');
        return $m;
    }

    /**
     * Get a dummy implementation
     *
     * @return Aedart\Laravel\Database\Interfaces\ConnectionResolverAware
     */
    protected function getDummyImpl(){
        return new DummyConnectionResolverClass();
    }

    /**
     * Get a migrator mock
     *
     * @return Mockery\MockInterface|Illuminate\Database\ConnectionResolverInterface
     */
    protected function getConnectionResolverMock(){
        $m = Mockery::mock('Illuminate\Database\ConnectionResolverInterface');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultConnectionResolver
     * @covers ::getDefaultConnectionResolver
     */
    public function getNullAsDefaultConnectionResolver(){
        $trait = $this->getTraitMock();
        $this->assertFalse($trait->hasDefaultConnectionResolver());
        $this->assertNull($trait->getDefaultConnectionResolver());
    }

    /**
     * @test
     * @covers ::getConnectionResolver
     * @covers ::hasConnectionResolver
     * @covers ::hasDefaultConnectionResolver
     * @covers ::setConnectionResolver
     * @covers ::getDefaultConnectionResolver
     * @covers ::isConnectionResolverValid
     */
    public function getDefaultConnectionResolver(){
        $this->startApplication();

        $trait = $this->getTraitMock();
        $this->assertInstanceOf('Illuminate\Database\ConnectionResolverInterface', $trait->getConnectionResolver());

        $this->stopApplication();
    }

    /**
     * @test
     * @covers ::getConnectionResolver
     * @covers ::hasConnectionResolver
     * @covers ::setConnectionResolver
     * @covers ::isConnectionResolverValid
     */
    public function setAndGetConnectionResolver(){
        $trait = $this->getTraitMock();

        $resolver = $this->getConnectionResolverMock();

        $trait->setConnectionResolver($resolver);

        $this->assertSame($resolver, $trait->getConnectionResolver());
    }

    /**
     * @test
     * @covers ::getConnectionResolver
     * @covers ::hasConnectionResolver
     * @covers ::setConnectionResolver
     * @covers ::isConnectionResolverValid
     *
     * @expectedException \Aedart\Laravel\Database\Exceptions\InvalidConnectionResolverException
     */
    public function attemptSetInvalidConnectionResolver(){
        $dummy = $this->getDummyImpl();

        $resolver = $this->getConnectionResolverMock();

        $dummy->setConnectionResolver($resolver);
    }
}

class DummyConnectionResolverClass implements ConnectionResolverAware {
    use ConnectionResolverTrait;

    public function isConnectionResolverValid(ConnectionResolverInterface $resolver) {
        return false;
    }
}