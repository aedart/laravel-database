<?php  namespace Aedart\Laravel\Database\Exceptions; 

/**
 * Class Invalid Connection Resolver Exception
 *
 * Throw this exception when an invalid connection resolver has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Exceptions
 */
class InvalidConnectionResolverException extends \InvalidArgumentException{

}