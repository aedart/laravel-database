<?php  namespace Aedart\Laravel\Database\Traits;

use Aedart\Laravel\Database\Exceptions\InvalidConnectionResolverException;
use Illuminate\Database\ConnectionResolverInterface;
use Aedart\Laravel\Detector\ApplicationDetector;

/**
 * Trait Connection Resolver
 *
 * @see \Aedart\Laravel\Database\Interfaces\ConnectionResolverAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Traits
 */
trait ConnectionResolverTrait {

    /**
     * The connection resolver
     *
     * @var ConnectionResolverInterface|null
     */
    protected $connectionResolver = null;

    /**
     * Set the connection resolver
     *
     * @param ConnectionResolverInterface $resolver The connection resolver this component must use
     *
     * @return void
     *
     * @throws InvalidConnectionResolverException If given connection resolver is invalid
     */
    public function setConnectionResolver(ConnectionResolverInterface $resolver){
        if(!$this->isConnectionResolverValid($resolver)){
            throw new InvalidConnectionResolverException('This given connection resolver is invalid');
        }
        $this->connectionResolver = $resolver;
    }

    /**
     * Get the connection resolver
     *
     * If no connection resolver is available, this method sets and
     * returns a default connection resolver, if any is available
     *
     * @see getDefaultConnectionResolver()
     *
     * @return ConnectionResolverInterface|null The connection resolver of this component or null if none has been set
     */
    public function getConnectionResolver(){
        if(!$this->hasConnectionResolver() && $this->hasDefaultConnectionResolver()){
            $this->setConnectionResolver($this->getDefaultConnectionResolver());
        }
        return $this->connectionResolver;
    }

    /**
     * Get a default connection resolver, if any is available
     *
     * @return ConnectionResolverInterface|null A default connection resolver or null if none is available
     */
    public function getDefaultConnectionResolver(){
        $detector = new ApplicationDetector();
        if($detector->isApplicationAvailable()){
            return app('db');
        }
        return null;
    }

    /**
     * Check if a connection resolver has been set
     *
     * @return bool True if a connection resolver has been set, false if not
     */
    public function hasConnectionResolver(){
        if(!is_null($this->connectionResolver)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default connection resolver is available
     *
     * @return bool True if a default connection resolver is available, false if not
     */
    public function hasDefaultConnectionResolver(){
        if(!is_null($this->getDefaultConnectionResolver())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given connection resolver is valid, e.g. has one or more specific connections,
     * or has a default connection made available
     *
     * @param ConnectionResolverInterface $resolver The connection resolver to be validated
     *
     * @return bool True if the given resolver is valid, false if not
     */
    public function isConnectionResolverValid(ConnectionResolverInterface $resolver){
        // By default, we assume that everything is in order with the
        // given connection resolver - this validation is very component specific.
        // Thus, this should be overwritten is specific validation is needed
        return true;
    }

}