<?php  namespace Aedart\Laravel\Database\Migrations\Exceptions; 

/**
 * Class Invalid Migration Repository Exception
 *
 * Throw this exception when an invalid migration repository has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Exceptions
 */
class InvalidMigrationRepositoryException extends \InvalidArgumentException{

}