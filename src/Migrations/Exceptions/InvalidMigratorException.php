<?php  namespace Aedart\Laravel\Database\Migrations\Exceptions; 

/**
 * Class Invalid Migrator Exception
 *
 * Throw this exception when an invalid migrator has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Exceptions
 */
class InvalidMigratorException extends \InvalidArgumentException{

}