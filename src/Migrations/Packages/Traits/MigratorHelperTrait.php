<?php  namespace Aedart\Laravel\Database\Migrations\Packages\Traits;

use Aedart\Laravel\Database\Migrations\Packages\Exceptions\InvalidMigratorHelperException;
use Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper;
use Aedart\Laravel\Database\Migrations\Packages\MigratorHelper;

/**
 * Trait Migrator Helper
 *
 * @see \Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Packages\Traits
 */
trait MigratorHelperTrait {

    /**
     * The migrator helper
     *
     * @var IMigratorHelper|null
     */
    protected $migratorHelper = null;

    /**
     * Set the package migrator helper
     *
     * @param IMigratorHelper $helper The package migrator helper this component must use
     *
     * @return void
     *
     * @throws InvalidMigratorHelperException If the given package migrator is invalid
     */
    public function setMigratorHelper(IMigratorHelper $helper){
        if(!$this->isMigratorHelperValid($helper)){
            throw new InvalidMigratorHelperException('Invalid package migrator helper given; check if helper has a migrator set / available');
        }
        $this->migratorHelper = $helper;
    }

    /**
     * Get the package migrator helper
     *
     * If no helper has been set, this method sets and returns
     * a default package migrator helper, if any is available
     *
     * @see getDefaultMigratorHelper
     *
     * @return IMigratorHelper|null A migrator helper or null if none has been set
     */
    public function getMigratorHelper(){
        if(!$this->hasMigratorHelper() && $this->hasDefaultMigratorHelper()){
            $this->setMigratorHelper($this->getDefaultMigratorHelper());
        }
        return $this->migratorHelper;
    }

    /**
     * Get a default package migrator helper
     *
     * @return IMigratorHelper|null A default migrator helper or null if none is available
     */
    public function getDefaultMigratorHelper(){
        return new MigratorHelper();
    }

    /**
     * Check if a migrator helper has been set
     *
     * @return bool True if a migrator helper has been set, false if not
     */
    public function hasMigratorHelper(){
        if(!is_null($this->migratorHelper)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default migrator helper is available
     *
     * @return bool True if a default migrator helper is available, false if not
     */
    public function hasDefaultMigratorHelper(){
        if(!is_null($this->getDefaultMigratorHelper())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given migrator helper is valid, e.g. if it has a "correct" vendor
     * path set, if its migrator instance uses the correct connected, etc.
     *
     * @param IMigratorHelper $helper The helper to be validated
     *
     * @return bool True if the given helper is valid, false if not
     */
    public function isMigratorHelperValid(IMigratorHelper $helper){
        // In this case, we only wish to ensure that a migrator instance
        // is available on the given helper
        if($helper->hasMigrator() || $helper->hasDefaultMigrator()){
            return true;
        }
        return false;
    }

}