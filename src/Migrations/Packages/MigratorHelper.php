<?php  namespace Aedart\Laravel\Database\Migrations\Packages;

use Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper;
use Aedart\Laravel\Database\Migrations\Traits\MigratorTrait;
use Aedart\Model\Vendor\Path\Traits\ComposerVendorPathTrait;
use Symfony\Component\Finder\Finder;

/**
 * Class Migrator Helper
 *
 * This Migrator helper assumes that by default, composer's 'vendor-dir' is the
 * location where packages have been installed
 *
 * @see \Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper
 * @see \Aedart\Model\Vendor\Path\Traits\ComposerVendorPathTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Packages
 */
class MigratorHelper implements IMigratorHelper{

    use MigratorTrait, ComposerVendorPathTrait;

    public function prepare(){
        // Get the migrator to be used
        $migrator = $this->getMigrator();

        // Prepare the database - create repository in database, if not already created
        if(!$migrator->repositoryExists()){
            $migrator->getRepository()->createRepository();
        }
    }

    public function runPackageMigrations(array $packageMigrationsPathList, $recursive = true, $pretend = false) {
        $migrationPathList = [];
        foreach($packageMigrationsPathList as $k => $migrationsPath){
            $migrationPathList[] = $this->getVendorPath() . '/' . $migrationsPath;
        }
        $this->runMigrationList($migrationPathList, $recursive, $pretend);
    }

    public function runPackageMigration($packageMigrationsPath, $recursive = true, $pretend = false) {
        $this->runPackageMigrations([$packageMigrationsPath], $recursive, $pretend);
    }

    public function runMigrationList(array $migrationPathList, $recursive = true, $pretend = false) {
        // In this implementation, we have 2 options;
        //
        // a) If recursive, we invoke the getMigrator()->run() for each nested directory (a lot of run calls, migration
        //      notes might get screwed up, when hitting empty nested folders)
        //
        // b) If recursive, we invoke the getMigrator()->runMigrationList for all the migration files found.
        //      However, this means that we are doing our own implementation of the run method, which is NOT
        //      future safe...
        //
        // Option (a) might not be that bad; we could filter out eventual 'Nothing to migrate' messages for empty
        // nested folders. However, consider the following scenario;
        //      A package divides / organises its migrations into several nested folders.
        //      Each migration has a valid data / time name, meaning that some migrations must run before others.
        //      If option (a) is used, there is a risk that some migration files will fail, e.g. foreign key constraints,
        //      because one or more migrations didn't execute before the given migration, due to its location in some other folder.
        //
        // Therefore, the only logical choice, is to do our own implementation of the getMigrator()->run() method, despite
        // the risk of future changes... Means that we have to maintain this our selves, rather than purely relying on
        // the Laravel developers alone.

        // Prepare the database - create repository in database, if not already created
        $this->prepare();

        // List of all the migration folder - incl. eventual nested if recursive
        $foldersList = $migrationPathList;

        // Check if recursive
        if($recursive === true){
            $foldersList = $this->getAllNestedDirectoriesFromList($migrationPathList);
        }

        // Get the list of migrations that have yet to be executed
        $migrationFilesToBeExecuted = $this->getAndLoadMigrationFiles($foldersList);

        // Finally, run the migration
        $this->getMigrator()->runMigrationList($migrationFilesToBeExecuted, $pretend);
    }

    public function runMigration($migrationPath, $recursive = true, $pretend = false) {
        $this->runMigrationList([$migrationPath], $recursive, $pretend);
    }

    public function rollBackLastMigration($pretend = false) {
        return $this->getMigrator()->rollback($pretend);
    }

    public function rollBackAllMigrations($pretend = false) {
        $amount = 0;

        while(true){
            $count = $this->rollBackLastMigration($pretend);

            $amount += $count;

            if($count == 0){
                break;
            }
        }

        return $amount;
    }

    public function getNotes() {
        return $this->getMigrator()->getNotes();
    }

    /**
     * Get all the nested directories found from inside the given target directories (top-level directories)
     *
     * @param array $directoriesList List of top-level directories
     * @param bool $includeGivenDirectories [Optional][Default true] Includes the given directories in output
     *
     * @return string[] List of all nested directories - eventually also includes the top-level directories that are given
     */
    protected function getAllNestedDirectoriesFromList(array $directoriesList, $includeGivenDirectories = true){
        $directories = [];
        foreach($directoriesList as $k => $topLevelDirectory){
            if($includeGivenDirectories){
                $directories[] = $topLevelDirectory;
            }

            $directories = array_merge($directories, $this->getAllNestedDirectoriesFrom($topLevelDirectory));
        }
        return $directories;
    }

    /**
     * TODO: ... this should be moved in a separate package, because its very useful!
     * TODO: ... method is 99% based on Illuminate\Filesystem\Filesystem::directories() - should perhaps extend it?
     *
     * Get all the nested directories found from inside the given target directory
     *
     * @param string $directory Full path to the target directory
     *
     * @return string[] List of all nested directories, found inside the given directory
     */
    protected function getAllNestedDirectoriesFrom($directory){
        $directories = [];

        foreach (Finder::create()->in($directory)->directories() as $dir){
            $directories[] = $dir->getPathname();
        }

        return $directories;
    }

    /**
     * Load the migration files (which have yet not been executed) from the given
     * directories and return a list of files that need to be migrated
     *
     * @param array $directories List of directories in which migration files are located
     *
     * @return array List of migrations files that need to be executed / migrated
     */
    protected function getAndLoadMigrationFiles(array $directories){
        // Get the migrator to be used
        $migrator = $this->getMigrator();

        // Get a list of already executed migrations
        $previousExecutedMigrations = $migrator->getRepository()->getRan();

        // Migration files list
        $migrationFilesList = [];

        // Get all the files from 'all' the folders
        foreach($directories as $key => $folder){
            $files = $migrator->getMigrationFiles($folder);
            $migrationFilesList = array_merge($migrationFilesList, $files);

            // Composer's autoload -> "class map" is not recursive, thus, we do need to ensure that migrations are loaded!
            $this->loadMigrationFiles($folder, $files, $previousExecutedMigrations);
        }

        // Sort the migration files (acc. to their names, which should be in data / time format...)
        sort($migrationFilesList);

        // Get the list of migrations that have yet to be executed
        $migrationFilesToBeExecuted = array_diff($migrationFilesList, $previousExecutedMigrations);

        // Finally, return the list of files to be migrated
        return $migrationFilesToBeExecuted;
    }

    /**
     * Load the files that need to be migrated
     *
     * @param string $directory Folder where the given files are located
     * @param array $files The list of files
     * @param array $alreadyMigratedFiles List of files already migrated
     */
    protected function loadMigrationFiles($directory, array $files, array $alreadyMigratedFiles){
        foreach($files as $file){
            // Skip loading if file already migrated
            if(in_array($file, $alreadyMigratedFiles)){
                continue;
            }

            // Originally, the migrator can handle loading (multiple files) - however, here we are just sending
            // a single file in an array, which is a bit slow. Therefore, we avoid using the migrator for this
            //$this->getMigrator()->requireFiles($directory, $filesToLoad);

            // Thus, we load in the given file
            $this->requireFile($directory . '/' . $file . '.php');
        }
    }

    /**
     * Require the given file
     *
     * @param string $fullPath The full path to the php-file, incl. file ext!
     */
    protected function requireFile($fullPath){
        $filesystem = $this->getMigrator()->getFilesystem();
        $filesystem->requireOnce($fullPath);
    }
}