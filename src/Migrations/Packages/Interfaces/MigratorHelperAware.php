<?php  namespace Aedart\Laravel\Database\Migrations\Packages\Interfaces; 

use Aedart\Laravel\Database\Migrations\Packages\Exceptions\InvalidMigratorHelperException;
use Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper;

/**
 * Interface Migration Helper Aware
 *
 * Components that implement this, are aware of and able to set a <b>package migrator helper</b>.
 * If no helper has been set, a default helper is made available, if any
 * is available (<i>implementation dependent</i>)
 *
 * @see \Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Packages\Interfaces
 */
interface MigratorHelperAware {

    /**
     * Set the package migrator helper
     *
     * @param IMigratorHelper $helper The package migrator helper this component must use
     *
     * @return void
     *
     * @throws InvalidMigratorHelperException If the given package migrator is invalid
     */
    public function setMigratorHelper(IMigratorHelper $helper);

    /**
     * Get the package migrator helper
     *
     * If no helper has been set, this method sets and returns
     * a default package migrator helper, if any is available
     *
     * @see getDefaultMigratorHelper
     *
     * @return IMigratorHelper|null A migrator helper or null if none has been set
     */
    public function getMigratorHelper();

    /**
     * Get a default package migrator helper
     *
     * @return IMigratorHelper|null A default migrator helper or null if none is available
     */
    public function getDefaultMigratorHelper();

    /**
     * Check if a migrator helper has been set
     *
     * @return bool True if a migrator helper has been set, false if not
     */
    public function hasMigratorHelper();

    /**
     * Check if a default migrator helper is available
     *
     * @return bool True if a default migrator helper is available, false if not
     */
    public function hasDefaultMigratorHelper();

    /**
     * Check if the given migrator helper is valid, e.g. if it has a "correct" vendor
     * path set, if its migrator instance uses the correct connected, etc.
     *
     * @param IMigratorHelper $helper The helper to be validated
     *
     * @return bool True if the given helper is valid, false if not
     */
    public function isMigratorHelperValid(IMigratorHelper $helper);

}