<?php  namespace Aedart\Laravel\Database\Migrations\Packages\Interfaces;

use Aedart\Laravel\Database\Migrations\Interfaces\MigratorAware;
use Aedart\Model\Vendor\Path\Interfaces\VendorPathAware;

/**
 * Interface Migrator Helper
 *
 * This helper offers various migrations methods, which allow you to specify package migrations folders,
 * in which migration files are to be executed from
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Packages\Interfaces
 */
interface IMigratorHelper extends MigratorAware, VendorPathAware{

    /**
     * Prepare for running a migration
     *
     * @return void
     */
    public function prepare();

    /**
     * Run the migration files from each of the given packages migration folders
     *
     * <b>recursive</b> If the recursive param is set to true, this method will search for
     * migration files in eventual nested directories, for each of the given migration paths
     * and migrate them as well
     *
     * <b>Example</b>
     *
     * <br />
     *
     * <pre><code>
     *  // Run all the migrations found inside;
     *  // vendor/acme/src/migrations
     *  // vendor/plugins/src/migrations
     *  // vendor/myCompany/src/migrations
     *  $migrationsHelper->runPackageMigrations([
     *      'acme/src/migrations',
     *      'plugins/src/migrations',
     *      'myCompany/src/migrations',
     *  ]);
     *
     *  // Run all the migrations for the given packages, inside the custom vendor folder ('lib' in this example)
     *  $migrationsHelper->setVendorPath('/lib');
     *  $migrationsHelper->runPackageMigrations([
     *      'acme/src/migrations',
     *      'plugins/src/migrations',
     *      'myCompany/src/migrations',
     *  ]);
     * </code></pre>
     *
     * @param string[] $packageMigrationsPathList List of package migration paths
     * @param bool $recursive [Optional][Default true] If true, method will search for migration files inside eventual nested directories - for each of the given migration paths
     * @param bool $pretend [Optional][Default false] Dump the SQL queries that would be run.
     *
     * @see setVendorPath()
     * @see getVendorPath()
     *
     * @return void
     */
    public function runPackageMigrations(array $packageMigrationsPathList, $recursive = true, $pretend = false);

    /**
     * Run the migration files from the given package's migrations folder
     *
     * <br />
     *
     * <b>recursive</b> If the recursive param is set to true, this method will search for
     * migration files in eventual nested directories and migrate them as well
     *
     * <b>Example</b>
     *
     * <br />
     *
     * <pre><code>
     *  // Run all the migrations found inside the /vendor/acme/src/migrations
     *  $migrationsHelper->runPackageMigration('acme/src/migrations');
     *
     *  // Run all the migration found inside /myCustomVendorLib/acme/src/migrations
     *  $migrationsHelper->setVendorPath('/myCustomVendorLib');
     *  $migrationsHelper->runPackageMigration('acme/src/migrations');
     * </code></pre>
     *
     * @param string $packageMigrationsPath Relative path (from vendor folder) to the package's migrations folder
     * @param bool $recursive [Optional][Default true] If true, method will search for migration files inside eventual nested directories
     * @param bool $pretend [Optional][Default false] Dump the SQL queries that would be run.
     *
     * @see setVendorPath()
     * @see getVendorPath()
     *
     * @return void
     */
    public function runPackageMigration($packageMigrationsPath, $recursive = true, $pretend = false);

    /**
     * Run the migration files that are located in each of the given directories
     *
     * <b>recursive</b> If the recursive param is set to true, this method will search for
     * migration files in eventual nested directories, for each of the given migration paths
     * and migrate them as well
     *
     * @param string[] $migrationPathList List of filesystem directory paths where migration files are located
     * @param bool $recursive [Optional][Default true] If true, method will search for migration files inside eventual nested directories - for each of the given migration paths
     * @param bool $pretend [Optional][Default false] Dump the SQL queries that would be run.
     *
     * @return void
     */
    public function runMigrationList(array $migrationPathList, $recursive = true, $pretend = false);

    /**
     * Run the migrations files that are located inside the given directory folder
     *
     * <b>recursive</b> If the recursive param is set to true, this method will search for
     * migration files in eventual nested directories and migrate them as well
     *
     * @param string $migrationPath Full filesystem directory path where migration files are located
     * @param bool $recursive [Optional][Default true] If true, method will search for migration files inside eventual nested directories
     * @param bool $pretend [Optional][Default false] Dump the SQL queries that would be run.
     *
     * @return void
     */
    public function runMigration($migrationPath, $recursive = true, $pretend = false);

    /**
     * Roll back the last migration that has been executed
     *
     * @param bool $pretend [Optional][Default false] Dump the SQL queries that would be run.
     *
     * @return int The amount of migrations that have been rolled back
     */
    public function rollBackLastMigration($pretend = false);

    /**
     * Roll back all the migrations that have been executed
     *
     * @param bool $pretend [Optional][Default false] Dump the SQL queries that would be run.
     *
     * @return int The amount of migrations that have been rolled back
     */
    public function rollBackAllMigrations($pretend = false);

    /**
     * Get the notes for the last operation.
     *
     * @return array
     */
    public function getNotes();
}