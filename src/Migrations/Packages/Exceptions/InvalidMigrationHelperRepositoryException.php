<?php  namespace Aedart\Laravel\Database\Migrations\Packages\Exceptions; 

/**
 * Class Invalid Migration-Helper Repository Exception
 *
 * Throw this exception when an invalid migration-helper repository has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Packages\Exceptions
 */
class InvalidMigrationHelperRepositoryException extends \InvalidArgumentException{

}