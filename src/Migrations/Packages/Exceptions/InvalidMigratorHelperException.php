<?php  namespace Aedart\Laravel\Database\Migrations\Packages\Exceptions; 

/**
 * Class Invalid Migrator Helper Exception
 *
 * Throw this exception when an invalid migrator helper has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Packages\Exceptions
 */
class InvalidMigratorHelperException extends \InvalidArgumentException{

}