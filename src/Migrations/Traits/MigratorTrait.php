<?php  namespace Aedart\Laravel\Database\Migrations\Traits;

use Aedart\Laravel\Database\Migrations\Exceptions\InvalidMigratorException;
use Aedart\Laravel\Detector\ApplicationDetector;
use Illuminate\Database\Migrations\Migrator;

/**
 * Trait Migrator
 *
 * @see \Aedart\Laravel\Database\Migrations\Interfaces\MigratorAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Traits
 */
trait MigratorTrait {

    /**
     * This component's migrator instance
     *
     * @var \Illuminate\Database\Migrations\Migrator|null
     */
    protected $migrator = null;

    /**
     * Set the migrator
     *
     * @param Migrator $migrator The migrator this component must use
     *
     * @return void
     *
     * @throws InvalidMigratorException If an invalid migrator has been provided
     */
    public function setMigrator(Migrator $migrator){
        if(!$this->isMigratorValid($migrator)){
            throw new InvalidMigratorException('The given migrator is invalid');
        }
        $this->migrator = $migrator;
    }

    /**
     * Get the migrator
     *
     * If no migrator has been set, then this method sets and
     * returns a default migrator, if any is available
     *
     * @see getDefaultMigrator()
     *
     * @return \Illuminate\Database\Migrations\Migrator|null This component's migrator to be used or null if none set / available
     */
    public function getMigrator(){
        if(!$this->hasMigrator() && $this->hasDefaultMigrator()){
            $this->setMigrator($this->getDefaultMigrator());
        }
        return $this->migrator;
    }

    /**
     * Get a default migrator, if any is available
     *
     * @return \Illuminate\Database\Migrations\Migrator|null A default migrator that can be used or null if no default is available
     */
    public function getDefaultMigrator(){
        $detector = new ApplicationDetector();
        if($detector->isApplicationAvailable()){
            return app('migrator');
        }
        return null;
    }

    /**
     * Check if a migrator instance has been set
     *
     * @return bool True if a migrator has been set, false if not
     */
    public function hasMigrator(){
        if(!is_null($this->migrator)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default migrator is available
     *
     * @return bool True if a default migrator is available, false if not
     */
    public function hasDefaultMigrator(){
        if(!is_null($this->getDefaultMigrator())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given migrator is valid, e.g. if uses the desired migrations repository,
     * the correct connection resolver, or has some specific desired configuration
     *
     * @param Migrator $migrator The migrator to be validated
     *
     * @return bool True if the given migrator is valid, e.g. uses the desired migrations repository, connection resolver
     *              or other specific settings. False if the given migrator is not valid
     */
    public function isMigratorValid(Migrator $migrator){
        // By default, no validation is performed - this should
        // always be overwritten in concrete situations, if specific
        // validation is required, such as ensuring the correct connection
        // is being used, etc
        return true;
    }

}