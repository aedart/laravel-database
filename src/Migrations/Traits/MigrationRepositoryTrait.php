<?php  namespace Aedart\Laravel\Database\Migrations\Traits;

use Aedart\Laravel\Database\Migrations\Exceptions\InvalidMigrationRepositoryException;
use Illuminate\Database\Migrations\MigrationRepositoryInterface;
use Aedart\Laravel\Detector\ApplicationDetector;

/**
 * Trait Migration Repository
 *
 * @see \Aedart\Laravel\Database\Migrations\Interfaces\MigrationRepositoryAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Traits
 */
trait MigrationRepositoryTrait {

    /**
     * The migration repository
     *
     * @var MigrationRepositoryInterface|null
     */
    protected $migrationRepository = null;

    /**
     * Set the migration repository
     *
     * @param MigrationRepositoryInterface $repository The migration repository to be used by this component
     *
     * @return void
     *
     * @throws InvalidMigrationRepositoryException If given migration repository is invalid
     */
    public function setMigrationRepository(MigrationRepositoryInterface $repository){
        if(!$this->isMigrationRepositoryValid($repository)){
            throw new InvalidMigrationRepositoryException('Given migration repository is invalid');
        }
        $this->migrationRepository = $repository;
    }

    /**
     * Get the migration repository
     *
     * If no migration repository has been set, this method sets
     * and returns a default repository, if any is available
     *
     * @see getDefaultMigrationRepository(){}
     *
     * @return MigrationRepositoryInterface|null The migration repository used by this component or null if none has been set
     */
    public function getMigrationRepository(){
        if(!$this->hasMigrationRepository() && $this->hasDefaultMigrationRepository()){
            $this->setMigrationRepository($this->getDefaultMigrationRepository());
        }
        return $this->migrationRepository;
    }

    /**
     * Get a default migration repository, if any is available
     *
     * @return MigrationRepositoryInterface|null A default migration repository or null if none is available
     */
    public function getDefaultMigrationRepository(){
        $detector = new ApplicationDetector();
        if($detector->isApplicationAvailable()){
            return app('migration.repository');
        }
        return null;
    }

    /**
     * Check if a migration repository has been set
     *
     * @return bool True if a migration repository has been set, false if not
     */
    public function hasMigrationRepository(){
        if(!is_null($this->migrationRepository)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default migration repository is available
     *
     * @return bool True if a default migration repository is available, false if not
     */
    public function hasDefaultMigrationRepository(){
        if(!is_null($this->getDefaultMigrationRepository())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given migration repository is valid; e.g. if it has an existing repository created
     * or has a specific amount of migrations already stored, etc.
     *
     * Override this method, if your migration repository needs to be validated in some kind of way!
     *
     * @param MigrationRepositoryInterface $repository The repository to be validated
     *
     * @return bool True if the given migration repository is valid, false if not
     */
    public function isMigrationRepositoryValid(MigrationRepositoryInterface $repository){
        // By default, we assume that the given repository is valid.
        // In other words, any validation would be component specific and there is
        // no need to do so here...
        return true;
    }

}