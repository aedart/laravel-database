<?php  namespace Aedart\Laravel\Database\Migrations\Interfaces; 

use Aedart\Laravel\Database\Migrations\Exceptions\InvalidMigrationRepositoryException;
use Illuminate\Database\Migrations\MigrationRepositoryInterface;

/**
 * Interface Migration Repository Aware
 *
 * Components that implement this, are able to specify a migration repository.
 * A default repository might be returned, if such is available.
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Interfaces
 */
interface MigrationRepositoryAware {

    /**
     * Set the migration repository
     *
     * @param MigrationRepositoryInterface $repository The migration repository to be used by this component
     *
     * @return void
     *
     * @throws InvalidMigrationRepositoryException If given migration repository is invalid
     */
    public function setMigrationRepository(MigrationRepositoryInterface $repository);

    /**
     * Get the migration repository
     *
     * If no migration repository has been set, this method sets
     * and returns a default repository, if any is available
     *
     * @see getDefaultMigrationRepository();
     *
     * @return MigrationRepositoryInterface|null The migration repository used by this component or null if none has been set
     */
    public function getMigrationRepository();

    /**
     * Get a default migration repository, if any is available
     *
     * @return MigrationRepositoryInterface|null A default migration repository or null if none is available
     */
    public function getDefaultMigrationRepository();

    /**
     * Check if a migration repository has been set
     *
     * @return bool True if a migration repository has been set, false if not
     */
    public function hasMigrationRepository();

    /**
     * Check if a default migration repository is available
     *
     * @return bool True if a default migration repository is available, false if not
     */
    public function hasDefaultMigrationRepository();

    /**
     * Check if the given migration repository is valid; e.g. if it has an existing repository created
     * or has a specific amount of migrations already stored, etc.
     *
     * Override this method, if your migration repository needs to be validated in some kind of way!
     *
     * @param MigrationRepositoryInterface $repository The repository to be validated
     *
     * @return bool True if the given migration repository is valid, false if not
     */
    public function isMigrationRepositoryValid(MigrationRepositoryInterface $repository);
}