<?php  namespace Aedart\Laravel\Database\Migrations\Interfaces; 

use Aedart\Laravel\Database\Migrations\Exceptions\InvalidMigratorException;
use Illuminate\Database\Migrations\Migrator;

/**
 * Interface Migrator Aware
 *
 * Components that implement this, promise that a migrator can be specified
 * and obtained again, when it is needed. Furthermore, depending upon implementation, a
 * default migrator might be available, if none has been specified prior to obtaining
 * it.
 *
 * @see \Illuminate\Database\Migrations\Migrator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Migrations\Interfaces
 */
interface MigratorAware {

    /**
     * Set the migrator
     *
     * @param Migrator $migrator The migrator this component must use
     *
     * @return void
     *
     * @throws InvalidMigratorException If an invalid migrator has been provided
     */
    public function setMigrator(Migrator $migrator);

    /**
     * Get the migrator
     *
     * If no migrator has been set, then this method sets and
     * returns a default migrator, if any is available
     *
     * @see getDefaultMigrator()
     *
     * @return \Illuminate\Database\Migrations\Migrator|null This component's migrator to be used or null if none set / available
     */
    public function getMigrator();

    /**
     * Get a default migrator, if any is available
     *
     * @return \Illuminate\Database\Migrations\Migrator|null A default migrator that can be used or null if no default is available
     */
    public function getDefaultMigrator();

    /**
     * Check if a migrator instance has been set
     *
     * @return bool True if a migrator has been set, false if not
     */
    public function hasMigrator();

    /**
     * Check if a default migrator is available
     *
     * @return bool True if a default migrator is available, false if not
     */
    public function hasDefaultMigrator();

    /**
     * Check if the given migrator is valid, e.g. if uses the desired migrations repository,
     * the correct connection resolver, or has some specific desired configuration
     *
     * @param Migrator $migrator The migrator to be validated
     *
     * @return bool True if the given migrator is valid, e.g. uses the desired migrations repository, connection resolver
     *              or other specific settings. False if the given migrator is not valid
     */
    public function isMigratorValid(Migrator $migrator);

}