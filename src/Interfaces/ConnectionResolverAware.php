<?php  namespace Aedart\Laravel\Database\Interfaces;

use Aedart\Laravel\Database\Exceptions\InvalidConnectionResolverException;
use Illuminate\Database\ConnectionResolverInterface;

/**
 * Interface Connection Resolver Aware
 *
 * Components that implement this, are able to specify and retrieve a connection resolver.
 * Furthermore, a default connection resolver is returned, is any is available
 *
 * @see \Illuminate\Database\ConnectionResolverInterface
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Laravel\Database\Interfaces
 */
interface ConnectionResolverAware {

    /**
     * Set the connection resolver
     *
     * @param ConnectionResolverInterface $resolver The connection resolver this component must use
     *
     * @return void
     *
     * @throws InvalidConnectionResolverException If given connection resolver is invalid
     */
    public function setConnectionResolver(ConnectionResolverInterface $resolver);

    /**
     * Get the connection resolver
     *
     * If no connection resolver is available, this method sets and
     * returns a default connection resolver, if any is available
     *
     * @see getDefaultConnectionResolver()
     *
     * @return ConnectionResolverInterface|null The connection resolver of this component or null if none has been set
     */
    public function getConnectionResolver();

    /**
     * Get a default connection resolver, if any is available
     *
     * @return ConnectionResolverInterface|null A default connection resolver or null if none is available
     */
    public function getDefaultConnectionResolver();

    /**
     * Check if a connection resolver has been set
     *
     * @return bool True if a connection resolver has been set, false if not
     */
    public function hasConnectionResolver();

    /**
     * Check if a default connection resolver is available
     *
     * @return bool True if a default connection resolver is available, false if not
     */
    public function hasDefaultConnectionResolver();

    /**
     * Check if the given connection resolver is valid, e.g. has one or more specific connections,
     * or has a default connection made available
     *
     * @param ConnectionResolverInterface $resolver The connection resolver to be validated
     *
     * @return bool True if the given resolver is valid, false if not
     */
    public function isConnectionResolverValid(ConnectionResolverInterface $resolver);
}