## Laravel-Database ##

Various utilities and helpers for Laravel's Database package, here among a package migrations helper.

## Contents ##

[TOC]

## How to install ##

For Laravel version 5.0.x

```
#!console

composer require aedart/laravel-database
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Package Migrations Helper ##

### What is this? ###

The package migrations helper is a utility that allows you to run migrations directly from the `vendor` directory. It serves as an alternative to Laravel's native migrator.  

Nevertheless, I do recommend that you use Laravel's [Service Providers](http://laravel.com/docs/5.0/providers) in order to [publish your package's migrations](http://laravel.com/docs/5.0/packages#publishing-file-groups), rather than migrating purely from
the `vendor` folder.
 
### Prerequisite ###

You should be running your application within a Laravel framework

Or

You are using some kind of testing framework like [orchestral-testbench](https://github.com/orchestral/testbench) which makes the Laravel application available in your tests

#### Composer classmap ####

Before you are using the helper, you should declare a [`classmap`](https://getcomposer.org/doc/04-schema.md#classmap) of where your package migrations are -
unless the given packages already have declared such or your migration files are namespaced. 

*Example*

```
#!json
{
    "autoload": {
        "classmap": [
            "vendor/acme/db/src/migrations",
            "vendor/acme/plugin/src/myCustomMigrations",
            "vendor/acme/runner/src/db-migrations"
        ]
    }
}
```

*If you do not do this, then you might not be able to rollback executed migrations*

### MigratorHelper and MigratorHelperTrait ###

If you have migrations that need to be executed from various locations, then you can use the `MigratorHelper` and or the `MigratorHelperTrait` to help you migrate.

#### From default vendor directory ####

The example below, assumes that you have a default composer configuration, in which the `vendor` directory is just called vendor!

```
#!php
<?php

use Aedart\Laravel\Database\Migrations\Packages\Traits\MigratorHelperTrait;

// Example class
class MyMigrationInvoker {

    use MigratorHelperTrait;

    public function run(){
        // Run the migrations, recursively find all migration files in nested directories
        // from /vendor/... 
        $this->getMigratorHelper()->runPackageMigrations([
            'acme/db/src/migrations',
            'acme/plugin/src/MyMigrations',
        ]);
    }

}
```

#### Custom vendor directory ####

If you have a different `vendor` directory configured in your top-level composer file, then you can specify its location using the `setVendorPath` method.

```
#!php
<?php

use Aedart\Laravel\Database\Migrations\Packages\Traits\MigratorHelperTrait;

// Example class
class MyMigrationInvoker {

    use MigratorHelperTrait;

    public function run(){
        // Run the migrations, recursively find all migration files in nested directories
        // from /home/etc/lib/myCustomVendorLib/...
        $this->getMigratorHelper()->setVendorPath('/home/etc/lib/myCustomVendorLib')
        $this->getMigratorHelper()->runPackageMigrations([
            'acme/db/src/migrations',
            'acme/plugin/src/MyMigrations',
        ]);
    }

}
```

#### Rollback last migration and Rollback all migrations ####

You can use the `rollBackLastMigration` method to roll back the last executed migration.

*NB:* Make sure that the migration files have been class-mapped via composer [`autoload`](https://getcomposer.org/doc/04-schema.md#classmap) or you might not be able to rollback
any previously executed migrations.

```
#!php
<?php

use Aedart\Laravel\Database\Migrations\Packages\Traits\MigratorHelperTrait;

// Example class
class MyMigrationInvoker {

    use MigratorHelperTrait;

    public function back(){
        // Roll back last migration
        $this->getMigratorHelper()->rollBackLastMigration();
    }

}
```

Or ... You can use the `rollBackAllMigrations` method to roll back all executed migrations.

```
#!php
<?php

use Aedart\Laravel\Database\Migrations\Packages\Traits\MigratorHelperTrait;

// Example class
class MyMigrationInvoker {

    use MigratorHelperTrait;

    public function back(){
        // Roll back last migration
        $this->getMigratorHelper()->rollBackAllMigrations();
    }

}
```

#### Recursive vs. Nonrecursive ####

All `run-migration` methods offered by the `MigratorHelper` are by default recursive!
 
This means that you can organise your migration files in nested sub-directories, as you see fit. 

If you do not wish to run migrations recursively, then you can specify the `$recursive` option. on the desired method.

**Example structure**
```
+ vendor
    + acme
        + sales
            + src
                + migrations
                    + products
                        - 2015_04_120000_create_products_table.php
                    + sales
                        - 2015_04_10_121000_create_statistics_table.php
                    - 2015_04_120500_create_sales_table.php
```

Given the above migration-files structure, you can run the migrations recursively or you can set the 2nd argument to `false` (the `$recursive` option), for the "run migration" methods.

```
#!php
<?php

use Aedart\Laravel\Database\Migrations\Packages\MigratorHelper;

$helper = new MigratorHelper();

// By default, migrations are executed recursively
//$helper->runPackageMigration('acme/sales/src/migrations');

// This method does NOT execute all the migration files - it doesn't run recursively
$helper->runPackageMigration('acme/sales/src/migrations', false);

```

For additional information, please review the `Aedart\Laravel\Database\Migrations\Packages\Interfaces\IMigratorHelper` interface.

## Acknowledgement ##

[Taylor Otwell et al.](http://laravel.com/) for one of the best PHP frameworks ever created.

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package
